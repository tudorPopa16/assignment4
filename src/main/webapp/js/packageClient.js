$(document).ready(
		function() {

			jQuery.support.cors = true;

			$.ajax({
				type : "GET",
				url : "http://localhost:8080/Ass2/PackageClientServlet",
				dataType : "json",
				success : function(data) {

					var trHTML = '';

					$.each(data, function(i, item) {

						trHTML += '<tr><td>' + data[i].idPackage + '</td><td>'
								+ data[i].sender.username + '</td><td>'
								+ data[i].receiver.username + '</td><td>'
								+ data[i].name + '</td><td>'
								+ data[i].description + '</td><td>'
								+ data[i].senderCity + '</td><td>'
								+ data[i].destinationCity + '</td><td>'
								+ data[i].tracking + '</td><td>'
								+ '<button name="idPackage" value="'
								+ data[i].idPackage
								+ '">Trace Package</button>' + '</td></tr>';

					});

					$("#package_table").append(trHTML);
					// console.log(JSON.stringify(data));
					
					
					$("button").click(function() {
						jQuery.support.cors = true;
						
						 $.ajax({
							 type : "GET",
							 url : "http://localhost:8080/Ass2/RouteServlet?idPackage=" + $(this).attr("value"),
							 dataType : "json",
							 success : function(data) {
						
								 var HTML = '<tr><th>City</th>'
									 	+ '<th>Time</th></tr>';
						
								 $.each(data, function(i, item) {
						
									 HTML += '<tr><td>'
										 + data[i].city
										 + '</td><td>'
										 + data[i].time
										 + '</td></tr>';
										
								 });
						
								 $("#trace_package").append("Trace Package");
								 $("#trace_table").append(HTML);
								 //console.log(JSON.stringify(data));
						
							 },
						
							 error : function(sls, msg) {
						
								 console.log(sls.responseText);
								 window.location.href = "index.html";
							 }
						 });
					});

				},

				error : function(sls, msg) {

					console.log(sls.responseText);
					window.location.href = "index.html";
				}
			});
			
		});