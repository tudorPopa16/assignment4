$(document).ready(
	function() {
	
		jQuery.support.cors = true;
	
		$.ajax({
			type : "GET",
			url : "http://localhost:8080/Ass2/PackageServlet",
			dataType : "json",
			success : function(data) {
	
				var trHTML = '';
				var selectHTML = '';
	
				$.each(data, function(i, item) {
	
					trHTML += '<tr><td>' + data[i].idPackage
							+ '</td><td>'
							+ data[i].sender.username
							+ '</td><td>'
							+ data[i].receiver.username
							+ '</td><td>'
							+ data[i].name
							+ '</td><td>'
							+ data[i].description
							+ '</td><td>'
							+ data[i].senderCity
							+ '</td><td>'
							+ data[i].destinationCity
							+ '</td><td>'
							+ data[i].tracking
							+ '</td><td>'
							+ '<form action="PackageServlet" method="GET"><button name="idPackage" value="'+ data[i].idPackage + '">Delete</button></form>'
							+ '</td></tr>';
					
					selectHTML += "<option>" + data[i].idPackage + "</option>";
					
				});
	
				$("#dropdown").append(selectHTML);
				$("#package_table").append(trHTML);
				//console.log(JSON.stringify(data));
	
			},
	
			error : function(sls, msg) {
	
				console.log(sls.responseText);
				window.location.href = "index.html";
			}
		});
	});