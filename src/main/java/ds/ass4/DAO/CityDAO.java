package ds.ass4.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ds.ass4.entities.City;
import ds.ass4.util.HibernateUtil;

public class CityDAO {
	
	private static CityDAO cityDAO = null;

	protected CityDAO() {
		// Exists only to defeat instantiation.
	}

	public static CityDAO getInstance() {
		if (cityDAO == null) {
			cityDAO = new CityDAO();
		}
		return cityDAO;
	}
	
	@SuppressWarnings("unchecked")
	public List<City> list(){
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		List<City> list = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("from City");
			list = (List<City>) query.list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}
	
	public City getCityByName(String name){
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		City list = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("from City where name = :name");
			query.setParameter("name", name);
			list = (City) query.uniqueResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

}
