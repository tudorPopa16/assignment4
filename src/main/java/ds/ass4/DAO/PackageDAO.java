package ds.ass4.DAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ds.ass4.entities.Package;
import ds.ass4.util.HibernateUtil;

public class PackageDAO {

	private static PackageDAO packageDAO = null;

	protected PackageDAO() {
		// Exists only to defeat instantiation.
	}

	public static PackageDAO getInstance() {
		if (packageDAO == null) {
			packageDAO = new PackageDAO();
		}
		return packageDAO;
	}

	public void saveOrUpdate(Package package_) {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(package_);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Package> list() {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		List<Package> list = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("from Package");
			list = (List<Package>) query.list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Package> getPackagesByUsername(String username) {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		List<Package> packages = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("from Package where sender.username = :sender");
			query.setParameter("sender", username);
			packages = (List<Package>)query.list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return packages;
	}

	public void deletePackage(int id) {
		Package package_ = getPackageById(id);
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(package_);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
	}

	public Package getPackageById(int id) {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		Package package_ = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("from Package where idPackage = :idPackage");
			query.setParameter("idPackage", id);
			package_ = (Package) query.uniqueResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return package_;
	}

}
