package ds.ass4.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ds.ass4.entities.Route;
import ds.ass4.util.HibernateUtil;

public class RouteDAO {
	
	private static RouteDAO routeDAO = null;

	protected RouteDAO() {
		// Exists only to defeat instantiation.
	}

	public static RouteDAO getInstance() {
		if (routeDAO == null) {
			routeDAO = new RouteDAO();
		}
		return routeDAO;
	}
	
	public void saveOrUpdate(Route route) {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(route);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Route> listByIdPackage(int idPackage) {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		List<Route> list = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("from Route where package_.idPackage = :idPackage");
			query.setParameter("idPackage", idPackage);
			list = (List<Route>) query.list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

}
