package ds.ass4.DAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ds.ass4.entities.Flight;
import ds.ass4.util.HibernateUtil;

public class FlightDAO {

	private static FlightDAO flightDAO = null;

	protected FlightDAO() {
		// Exists only to defeat instantiation.
	}

	public static FlightDAO getInstance() {
		if (flightDAO == null) {
			flightDAO = new FlightDAO();
		}
		return flightDAO;
	}

	@SuppressWarnings("unchecked")
	public List<Flight> list() {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		List<Flight> list = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("from Flight");
			list = (List<Flight>) query.list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

	public Flight getFlightById(int id) {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		Flight flight = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("from Flight where idFlight = :idFlight");
			query.setParameter("idFlight", id);
			flight = (Flight) query.uniqueResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flight;
	}
	
	public Flight getFlightByNo(int no) {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		Flight flight = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("from Flight where flightNumber = :flightNumber");
			query.setParameter("flightNumber", no);
			flight = (Flight) query.uniqueResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flight;
	}

	public void saveOrUpdate(Flight flight) {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(flight);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void deleteFlight(int id) {
		Flight flight = getFlightById(id);
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

	}

}
