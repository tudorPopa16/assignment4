package ds.ass4.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ds.ass4.entities.Flight;
import ds.ass4.util.HibernateUtil;

public class UserFlightDAO {
	
	private static UserFlightDAO userFlightDAO = null;

	protected UserFlightDAO() {
		// Exists only to defeat instantiation.
	}

	public static UserFlightDAO getInstance() {
		if (userFlightDAO == null) {
			userFlightDAO = new UserFlightDAO();
		}
		return userFlightDAO;
	}
	
	@SuppressWarnings("unchecked")
	public List<Flight> list() {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		List<Flight> list = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("from UserFlight");
			list = (List<Flight>) query.list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Flight> getFlightsByUserId(int id) {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		List<Flight> list = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("from UserFlight where user.idUser = :idUser");
			query.setParameter("idUser", id);
			list = (List<Flight>) query.list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

}
