package ds.ass4.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "flights")
public class Flight {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_flight")
	private int idFlight;

	@Column(name = "flight_number")
	private int flightNumber;

	@Column(name = "airplane_type")
	private String airplaneType;

	@ManyToOne
	@JoinColumn(name = "id_departure_city")
	private City departureCity;

	@Column(name = "departure_date_hour")
	private Date departureDateHour;

	@ManyToOne
	@JoinColumn(name = "id_arrival_city")
	private City arrivalCity;

	@Column(name = "arrival_date_hour")
	private Date arrivalDateHour;

	public int getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(int idFlight) {
		this.idFlight = idFlight;
	}

	public int getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public Date getDepartureDateHour() {
		return departureDateHour;
	}

	public void setDepartureDateHour(Date departureDateHour) {
		this.departureDateHour = departureDateHour;
	}

	public Date getArrivalDateHour() {
		return arrivalDateHour;
	}

	public void setArrivalDateHour(Date arrivalDateHour) {
		this.arrivalDateHour = arrivalDateHour;
	}

	public City getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(City departureCity) {
		this.departureCity = departureCity;
	}

	public City getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(City arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

}