package ds.ass4.Servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import ds.ass4.DAO.FlightDAO;
import ds.ass4.entities.City;
import ds.ass4.entities.Flight;
import ds.ass4.util.HTTPRequest;

/**
 * Servlet implementation class LocalTimeServlet
 */
public class LocalTimeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private FlightDAO flightDAO = FlightDAO.getInstance();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LocalTimeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int idFlight = Integer.parseInt(request.getParameter("idFlight"));
		Flight flight = flightDAO.getFlightById(idFlight);
		City departureCity = flight.getDepartureCity();
		City arrivalCity = flight.getArrivalCity();

		String urlDeparture = "http://api.geonames.org/timezoneJSON?lat=" + departureCity.getLatitude() + "&lng="
				+ departureCity.getLongitude() + "&username=tudor";
		String urlArrival = "http://api.geonames.org/timezoneJSON?lat=" + arrivalCity.getLatitude() + "&lng="
				+ arrivalCity.getLongitude() + "&username=tudor";
		String localArrivalHour = null;
		try {
			String responseDeparture = HTTPRequest.sendGet(urlDeparture);
			String responseArrival = HTTPRequest.sendGet(urlArrival);
			JSONObject jsonArrival = new JSONObject(responseArrival);
			JSONObject jsonDeparture = new JSONObject(responseDeparture);
			
			int gmtArrival = jsonArrival.getInt("gmtOffset");
			int gmtDeparture = jsonDeparture.getInt("gmtOffset");
			
			int gmtDifference = gmtArrival - gmtDeparture;
			
			System.out.println(gmtDifference);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(flight.getArrivalDateHour());
			cal.add(Calendar.HOUR_OF_DAY, gmtDifference);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			localArrivalHour = dateFormat.format(cal.getTime());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String style = "<style>table {font-family: arial, sans-serif; border-collapse: collapse; width: 75%; margin-bottom: 20px;} td, th {	border: 1px solid #dddddd;	text-align: left;	padding: 8px;}</style>";

		response.getWriter().append(style);
		
		String back = "<button onclick=\"window.location.href ='client.html'\">Back</button>";
		response.getWriter().append(back);
		
		String toDesplay = "<table>"+ 
				"<tr>" +
					"<th>Flight No.</th>" +
					"<th>Departure City</th>" +
					"<th>Departure Hour</th>" +
					"<th>Arrival City</th>" +
					"<th>Arrival Hour</th>" +
					"<th>Local Time</th>" +
				"</tr>" +
				"<tr>" +
					"<td>"+ flight.getFlightNumber() + "</td>" +
					"<td>"+ departureCity.getName() + "</td>" +
					"<td>"+ flight.getDepartureDateHour() + "</td>" +
					"<td>"+ arrivalCity.getName() + "</td>" +
					"<td>"+ flight.getArrivalDateHour() + "</td>" +
					"<td>"+ localArrivalHour + "</td>" +
				"</tr>" +
				"</table>";
		
		response.getWriter().append(toDesplay);
		
	}
	
	

}
