package ds.ass4.Servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import ds.ass4.DAO.CityDAO;
import ds.ass4.DAO.FlightDAO;
import ds.ass4.entities.City;
import ds.ass4.entities.Flight;

/**
 * Servlet implementation class Flights
 */
public class FlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private FlightDAO flightDAO = FlightDAO.getInstance();
	private CityDAO cityDAO = CityDAO.getInstance();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FlightServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getSession().getAttribute("role") == null
				|| !request.getSession().getAttribute("role").equals("admin")) {
			request.getRequestDispatcher("index.html").include(request, response);
		} else {

			if (request.getParameter("idFlight") == null) {
				// list flight
				String text = "";

				response.setContentType("text/json");
				response.setCharacterEncoding("UTF-8");

				List<Flight> list = flightDAO.list();
				text = new Gson().toJson(list);

				response.getWriter().write(text);
			} else {
				// delete flight
				int idFlight = Integer.parseInt(request.getParameter("idFlight"));
				flightDAO.deleteFlight(idFlight);
				response.sendRedirect("admin.html");
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Flight flight;
		int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
		flight = flightDAO.getFlightByNo(flightNumber);
		if (flight == null){
			flight = new Flight();
		}
		flight.setFlightNumber(flightNumber);
		flight.setAirplaneType(request.getParameter("airplaneType"));
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(request.getParameter("departureHour"));
			flight.setDepartureDateHour(date);
			Date date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(request.getParameter("arrivalHour"));
			flight.setArrivalDateHour(date1);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		String departureCity = request.getParameter("departureCity");
		String arrivalCity = request.getParameter("arrivalCity");

		System.out.println(departureCity + " " + arrivalCity);

		City cityD = cityDAO.getCityByName(departureCity);
		City cityA = cityDAO.getCityByName(arrivalCity);

		flight.setDepartureCity(cityD);
		flight.setArrivalCity(cityA);

		flightDAO.saveOrUpdate(flight);
		response.sendRedirect("admin.html");

	}

}
