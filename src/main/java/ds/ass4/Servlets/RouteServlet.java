package ds.ass4.Servlets;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import ds.ass4.DAO.PackageDAO;
import ds.ass4.DAO.RouteDAO;
import ds.ass4.entities.Package;
import ds.ass4.entities.Route;

/**
 * Servlet implementation class RouteServlet
 */
public class RouteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private PackageDAO packageDAO = PackageDAO.getInstance();
	private RouteDAO routeDAO = RouteDAO.getInstance();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RouteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String idPackageS = request.getParameter("idPackage");
		
		if (idPackageS != null) {
			int idPackage = Integer.parseInt(idPackageS);
			
			String text = "";

			response.setContentType("text/json");
			response.setCharacterEncoding("UTF-8");

			List<Route> packages = routeDAO.listByIdPackage(idPackage);

			text = new Gson().toJson(packages);
			response.getWriter().write(text);
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String city = request.getParameter("city");
		Date time = Date.valueOf(request.getParameter("time"));
		int id = Integer.parseInt(request.getParameter("id"));
		
		Package package_ = packageDAO.getPackageById(id);
		
		Route route = new Route();
		
		route.setCity(city);
		route.setTime(time);
		route.setPackage_(package_);
		
		package_.setTracking(true);
		packageDAO.saveOrUpdate(package_);
		
		routeDAO.saveOrUpdate(route);
		
		response.sendRedirect("package.html");
	}

}
