package ds.ass4.Servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import ds.ass4.DAO.UserDAO;
import ds.ass4.DAO.UserFlightDAO;
import ds.ass4.entities.Flight;
import ds.ass4.entities.User;

/**
 * Servlet implementation class UserServlet
 */
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UserDAO userDAO = UserDAO.getInstance();
	private UserFlightDAO userFlightDAO = UserFlightDAO.getInstance();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getSession().getAttribute("role") == null
				|| !request.getSession().getAttribute("role").equals("client")) {
			request.getRequestDispatcher("index.html").include(request, response);
		} else {

			String username = (String) request.getSession().getAttribute("user");
			User user = userDAO.getUserByUsername(username);
			
			String text = "";

			response.setContentType("text/json");
			response.setCharacterEncoding("UTF-8");

			List<Flight> list = userFlightDAO.getFlightsByUserId(user.getIdUser());
			text = new Gson().toJson(list);

			response.getWriter().write(text);

			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
