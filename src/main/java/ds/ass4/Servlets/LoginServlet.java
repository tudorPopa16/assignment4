package ds.ass4.Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ds.ass4.DAO.UserDAO;
import ds.ass4.entities.User;

/**
 * Servlet implementation class Hello
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UserDAO userDAO = UserDAO.getInstance();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("GET " + request.getParameter("username"));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		User user = userDAO.getUserByUsernameAndPassword(username, password);

		// Set response content type
		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		String show = "";

		if (user != null && user.getUsername().equals(username) && user.getPassword().equals(password)) {
			request.getSession().setAttribute("user", user.getUsername());
			request.getSession().setAttribute("role", user.getRole());
			if (user.getRole().equals("ADMIN")) {
				response.sendRedirect("package.html");
			} else {
				response.sendRedirect("client.html");
			}
		} else {
			show = "<h1>Login failed</h1>";
		}

		out.println(show);
	}

}
