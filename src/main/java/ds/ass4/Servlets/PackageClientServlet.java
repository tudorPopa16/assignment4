package ds.ass4.Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import ds.ass4.DAO.PackageDAO;
import ds.ass4.DAO.UserDAO;
import ds.ass4.entities.Package;
import ds.ass4.entities.User;

/**
 * Servlet implementation class PackageServlet
 */
public class PackageClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UserDAO userDAO = UserDAO.getInstance();
	private PackageDAO packageDAO = PackageDAO.getInstance();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PackageClientServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getSession().getAttribute("role") == null
				|| !request.getSession().getAttribute("role").equals("CLIENT")) {
			request.getRequestDispatcher("index.html").include(request, response);
		} else {

			String username = (String) request.getSession().getAttribute("user");

			// list all packages
			String text = "";

			response.setContentType("text/json");
			response.setCharacterEncoding("UTF-8");

			List<Package> packages = packageDAO.getPackagesByUsername(username);

			text = new Gson().toJson(packages);
			response.getWriter().write(text);
		}

	}
}
