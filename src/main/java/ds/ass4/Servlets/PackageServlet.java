package ds.ass4.Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import ds.ass4.DAO.PackageDAO;
import ds.ass4.DAO.UserDAO;
import ds.ass4.entities.Package;
import ds.ass4.entities.User;

/**
 * Servlet implementation class PackageServlet
 */
public class PackageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UserDAO userDAO = UserDAO.getInstance();
	private PackageDAO packageDAO = PackageDAO.getInstance();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PackageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getSession().getAttribute("role") == null
				|| !request.getSession().getAttribute("role").equals("ADMIN")) {
			request.getRequestDispatcher("index.html").include(request, response);
		} else {

			if (request.getParameter("idPackage") == null) {

				// list all packages
				String text = "";

				response.setContentType("text/json");
				response.setCharacterEncoding("UTF-8");

				List<Package> packages = packageDAO.list();

				text = new Gson().toJson(packages);
				response.getWriter().write(text);
			} else {
				int idPackage = Integer.parseInt(request.getParameter("idPackage"));
				try {
					packageDAO.deletePackage(idPackage);
					response.sendRedirect("package.html");
				} catch (Exception e) {
					String text = "The pakage does not exist";
					System.err.println(text);
					response.getWriter().write(text);
				}
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String sender = request.getParameter("sender");
		String receiver = request.getParameter("receiver");
		String name = request.getParameter("name");
		String description = request.getParameter("description");
		String senderCity = request.getParameter("senderCity");
		String destinationCity = request.getParameter("destinationCity");

		User senderClient = userDAO.getUserByUsername(sender);
		User receiverClient = userDAO.getUserByUsername(receiver);

		if (senderClient != null & receiverClient != null) {
			Package package_ = new Package();
			package_.setSender(senderClient);
			package_.setReceiver(receiverClient);
			package_.setName(name);
			package_.setDescription(description);
			package_.setSenderCity(senderCity);
			package_.setDestinationCity(destinationCity);

			packageDAO.saveOrUpdate(package_);
		} else {
			String text = "The clients does not exist";
			System.err.println(text);
			response.getWriter().write(text);
		}
		response.sendRedirect("package.html");

	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
