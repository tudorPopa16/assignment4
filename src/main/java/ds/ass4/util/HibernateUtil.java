package ds.ass4.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import ds.ass4.entities.City;
import ds.ass4.entities.Flight;
import ds.ass4.entities.Package;
import ds.ass4.entities.Route;
import ds.ass4.entities.User;
import ds.ass4.entities.UserFlight;

public class HibernateUtil {

	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			// load from different directory
			SessionFactory sessionFactory = new Configuration()
					.addAnnotatedClass(User.class)
					.addAnnotatedClass(City.class)
					.addAnnotatedClass(Flight.class)
					.addAnnotatedClass(UserFlight.class)
					.addAnnotatedClass(Package.class)
					.addAnnotatedClass(Route.class)
						.configure()
						.buildSessionFactory();

			return sessionFactory;

		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static Session openSession() {
        return sessionFactory.openSession();
    }
}
